package com.xie.springbootinit.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xie.springbootinit.model.entity.Category;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
