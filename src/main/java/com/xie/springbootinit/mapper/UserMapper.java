package com.xie.springbootinit.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xie.springbootinit.model.entity.SysPermission;
import com.xie.springbootinit.model.entity.SysRole;
import com.xie.springbootinit.model.entity.SysUser;
import com.xie.springbootinit.model.vo.user.UserVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户数据库操作
 *
 */
@Repository
public interface UserMapper extends BaseMapper<SysUser> {

    /**
     * 根据用户名获取用户信息
     * @param username
     * @return
     */
    SysUser findByUsername(@Param("value") String username);

    /**
     * 获取当前用户信息
     * @param user
     * @return
     */
    UserVo getUserInfo(SysUser user);

    /**
     * 根据用户ID查询权限信息
     * @param userId
     * @return
     */
    List<SysRole> findRoles(@Param("userId") Long userId);

    /**
     * 根据用户ID查询权限数据
     * @param userId
     * @return
     */
    List<SysPermission> findPermissions(@Param("userId") Long userId);


}




