package com.xie.springbootinit.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xie.springbootinit.model.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
}
