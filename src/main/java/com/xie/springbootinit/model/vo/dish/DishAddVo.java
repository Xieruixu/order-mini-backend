package com.xie.springbootinit.model.vo.dish;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author: Xrx
 * @Description:
 * @CreateTime: 2023/7/5 11:56
 */
@Data
public class DishAddVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 菜品名称
     */
    @NotBlank(message = "菜品名称不能为空")
    private String name;

    /**
     * 菜品描述
     */
    @Max(value = 100, message = "菜品描述不能超过100个字符")
    private String description;

    /**
     * 菜品分类名称
     */
    @NotBlank(message = "菜品分类名称不能为空")
    private String categoryName;


    /**
     * 菜品口味
     */
    @NotEmpty(message = "菜品口味不能为空")
    private List<String> dishFlavors;

    /**
     * 菜品价格
     */
    @NotNull(message = "菜品价格不能为空")
    @DecimalMin(value = "0.00", inclusive = false, message = "菜品价格必须大于0")
    private BigDecimal price;

    /**
     * 菜品图片
     */
    @NotNull(message = "菜品图片不能为空")
    private MultipartFile image;


}
