package com.xie.springbootinit.model.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Xrx
 * @Description
 * @CreateTime: 2023/2/12 16:48
 */
@Data
@ApiModel(value = "登录参数")
public class LoginVo {

    @ApiModelProperty(value = "用户名", dataType = "String")
    private String username;

    @ApiModelProperty(value = "密码", dataType = "String")
    private String password;

    @ApiModelProperty(value = "手机号码", dataType = "String")
    private String phoneNumber;

    @ApiModelProperty(value = "验证码", dataType = "String")
    private String code;

}
