package com.xie.springbootinit.model.vo.cart;

import com.xie.springbootinit.model.entity.DishFlavor;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author: Xrx
 * @Description:
 * @CreateTime: 2023/7/5 10:39
 */
@Data
public class CartVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 菜品id
     */
    private Long dishId;

    /**
     * 套餐id
     */
    private Long setmealId;

    /**
     * 口味
     */
    private List<DishFlavor> dishFlavors;

    /**
     * 数量
     */
    private Integer number;

    /**
     * 金额
     */
    private BigDecimal amount;


}
