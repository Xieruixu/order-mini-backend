package com.xie.springbootinit.model.vo.category;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: Xrx
 * @Description:
 * @CreateTime: 2023/7/4 20:58
 */
@Data
public class CategoryVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 类型(1:菜品分类 2:套餐分类)
     */
    private Integer type;


    /**
     * 分类名称
     */
    private String name;


}
