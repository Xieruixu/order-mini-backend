package com.xie.springbootinit.model.vo.order;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @Author: Xrx
 * @Description: 订单vo
 * @CreateTime: 2023/6/22 20:17
 */
@Data
public class OrderVo {

    /**
     * 订单编号
     */
    private String number;

    /**
     * 订单状态 0待支付，1已支付，2已取消
     */
    private Integer status;


    /**
     * 下单用户id
     */
    private Long userId;

    /**
     * 下单时间
     */
    private LocalDateTime orderTime;

    /**
     * 结账时间
     */
    private LocalDateTime checkoutTime;

    /**
     * 支付方式 0:微信 1:支付宝 2:银联
     */
    private Integer payMethod;

    /**
     * 实收金额
     */
    private BigDecimal amount;

    /**
     * 备注
     */
    private String remark;


}
