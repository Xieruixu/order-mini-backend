package com.xie.springbootinit.model.vo.common;

import com.xie.springbootinit.constant.CommonConstant;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 分页请求
 *
 * @author Xrx

 */
@Data
public class PageRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前页号
     */
    @NotNull(message = "当前页号不能为空")
    private long current = 1;

    /**
     * 页面大小
     */
    @NotNull(message = "页面大小不能为空")
    private long pageSize = 10;

    /**
     * 数据
     */

    private Object data;

    /**
     * 排序顺序（默认升序）
     */
    private String sortOrder = CommonConstant.SORT_ORDER_ASC;
}
