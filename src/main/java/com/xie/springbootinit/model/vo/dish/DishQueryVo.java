package com.xie.springbootinit.model.vo.dish;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author: Xrx
 * @Description:
 * @CreateTime: 2023/7/5 11:56
 */
@Data
public class DishQueryVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 菜品名称
     */
    private String name;

    /**
     * 菜品描述
     */
    private String description;

    /**
     * 菜品分类名称
     */
    private String categoryName;


    /**
     * 菜品口味
     */
    private List<String> dishFlavors;

    /**
     * 菜品价格
     */
    private BigDecimal price;


}
