package com.xie.springbootinit.model.vo.user;

import com.xie.springbootinit.model.entity.SysPermission;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 用户视图（脱敏）
 *
 */
@Data
public class UserVo implements Serializable {

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户名称
     */
    private String nickName;


    private Integer sex;

    private String avatar;

    private String address;

    private String openId;

    private boolean status;

    private boolean admin;

    private String phoneNumber;

    private String email;

    private List<SysPermission> permissions;

    private static final long serialVersionUID = 1L;
}