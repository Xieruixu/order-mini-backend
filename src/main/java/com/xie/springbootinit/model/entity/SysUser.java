package com.xie.springbootinit.model.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @Author Xrx
 * @Description 用户表
 * @CreateTime: 2023/2/12 16:37
 */
@Data
@NoArgsConstructor
@TableName("sys_user")
public class SysUser implements UserDetails {

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "用户名")
    @TableField(value = "user_name")
    private String userName;


    @ApiModelProperty(value = "登录密码")
    @TableField(value = "password")
    private String password;

    @ApiModelProperty(value = "用户昵称")
    @TableField(value = "nick_name")
    private String nickName;

    @ApiModelProperty(value = "性别")
    @TableField(value = "sex")
    private Integer sex;

    @ApiModelProperty(value = "头像")
    @TableField(value = "avatar")
    private String avatar;

    @ApiModelProperty(value = "地址")
    @TableField(value = "address")
    private String address;

    @ApiModelProperty(value = "微信唯一ID")
    @TableField(value = "open_id")
    private String openId;

    @ApiModelProperty(value = "是否禁用 0-未禁用 1-已禁用")
    @TableField(value = "status")
    private int status;

    @ApiModelProperty(value = "是否是管理员")
    @TableField(value = "admin")
    private boolean admin;

    @ApiModelProperty(value = "电话号码")
    @TableField(value = "phone_number")
    private String phoneNumber;

    @ApiModelProperty(value = "用户邮箱")
    @TableField(value = "email")
    private String email;

    @ApiModelProperty(value = "角色信息")
    private List<SysRole> roles;

    @ApiModelProperty(value = "用户对应的权限数据")
    private List<SysPermission> permissions;

    @ApiModelProperty(value = "是否删除")
    @TableField(value = "is_delete")
    @TableLogic
    private int isDelete;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time")
    private Date createdTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time")
    private Date updatedTime;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 权限数据
     * @JsonIgnore 在返回的数据中，将该方法对应的属性数据给排除
     * @return
     */
    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> list = new ArrayList<>();
        if (roles != null && roles.size() > 0) {
            roles.forEach(item -> {
                if (StringUtils.isNotEmpty(item.getCode())) {
                    list.add(new SimpleGrantedAuthority("ROLE_" + item.getCode()));
                }
            });
        }
        if (permissions != null && permissions.size() > 0) {
            permissions.forEach(item -> {
                if (StringUtils.isNotEmpty(item.getCode())) {
                    list.add(new SimpleGrantedAuthority(item.getCode()));
                }
            });
        }
        return list;
    }

    /**
     * 用户名
     * @return
     */
    @Override
    @JsonIgnore
    public String getUsername() {
        return userName;
    }

    /**
     * 账号是否过期
     * @return
     */
    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return false;
    }

    /**
     * 账号是否被锁定
     * @return
     */
    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return false;
    }

    /**
     * @return
     */
    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return false;
    }

    /**
     * 是否被禁用
     * @return
     */
    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return status == 0;
    }
}
