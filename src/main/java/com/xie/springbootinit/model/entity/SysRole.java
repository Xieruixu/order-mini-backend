package com.xie.springbootinit.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author Xrx
 * @Description 角色表
 * @CreateTime: 2023/2/12 16:37
 */
@Data
@TableName("sys_role")
public class SysRole {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;

    /**
     * 标签名称
     */
    @ApiModelProperty(value = "角色标签")
    private String label;

    /**
     * 标签值
     */
    @ApiModelProperty(value = "角色值")
    private String code;

    @ApiModelProperty(value = "显示状态(0不显示、1显示)")
    private boolean status;

    @ApiModelProperty(value = "权限列表")
    private List<SysPermission> permissions;
}
