package com.xie.springbootinit.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Xrx
 * @Description 数据权限
 * @CreateTime: 2023/2/12 16:37
 */
@Data
@TableName("sys_permission")
public class SysPermission {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;

    /**
     * 数据权限标签名称
     */
    @ApiModelProperty(value = "数据权限标签名称")
    private String label;

    /**
     * 数据权限标签值
     */
    @ApiModelProperty(value = "数据权限标签值")
    private String code;

    @ApiModelProperty(value = "显示状态(0不显示、1显示)")
    private boolean status;
}
