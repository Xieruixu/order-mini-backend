package com.xie.springbootinit.model.dto.user;

import lombok.Data;

/**
 * @Author: Xrx
 * @Description: 微信小程序登录请求
 * @CreateTime: 2023/5/16 21:11
 */
@Data
public class Code {
    private String code;
}
