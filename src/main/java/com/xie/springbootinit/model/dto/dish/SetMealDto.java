package com.xie.springbootinit.model.dto.dish;


import com.xie.springbootinit.model.entity.SetMeal;
import com.xie.springbootinit.model.entity.SetMealDish;
import lombok.Data;

import java.util.List;

@Data
public class SetMealDto {

    private List<SetMealDish> setMealDishes;

    private String categoryName;


}
