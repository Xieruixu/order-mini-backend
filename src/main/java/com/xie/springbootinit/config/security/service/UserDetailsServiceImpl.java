package com.xie.springbootinit.config.security.service;

import com.alibaba.fastjson.JSONArray;
import com.xie.springbootinit.base.utils.RedisUtils;
import com.xie.springbootinit.constant.UserConstant;
import com.xie.springbootinit.mapper.UserMapper;
import com.xie.springbootinit.model.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @Author Xrx
 * @Description
 * @CreateTime: 2023/2/14 15:18
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //判断缓存中是否存在用户信息 存在则直接从缓存中取，不存在则查询数据库并把数据存入缓存
        SysUser user;
        if (redisUtils.hasKey(UserConstant.USER_INFO + username)) {
            // 缓存中存在用户信息,直接从redis中取
            String userJson = redisUtils.getValue(UserConstant.USER_INFO + username).toString();
            user = JSONArray.parseObject(userJson, SysUser.class);
            redisUtils.expire(UserConstant.USER_INFO + username, 60);
        } else {
            user = userMapper.findByUsername(username);
            if (Objects.isNull(user)) {
                return null;
            }
            if (user.isAdmin()) {
                user.setRoles(userMapper.findRoles(null));
                user.setPermissions(userMapper.findPermissions(null));
            } else {
                user.setRoles(userMapper.findRoles(user.getId()));
                user.setPermissions(userMapper.findPermissions(user.getId()));
            }
            redisUtils.setValueTime(UserConstant.USER_INFO + username, user, 60);
        }
        return user;

    }



}
