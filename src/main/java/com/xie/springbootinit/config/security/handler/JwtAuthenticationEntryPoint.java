package com.xie.springbootinit.config.security.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.base.resultcode.LoginCode;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @Author Xrx
 * @Description 当用户未登录和token过期的情况下访问资源
 * @CreateTime: 2023/2/13 16:19
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        response.setStatus(401);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();
        writer.write(new ObjectMapper().writeValueAsString(BaseResponse.fail(LoginCode.UN_LOGIN_ERROR)));
        writer.flush();
        writer.close();
    }
}
