package com.xie.springbootinit.config.security.contents;

/**
 * @Author Xrx
 * @Description 白名单
 * @CreateTime: 2023/2/12 17:21
 */
public class SecurityContents {
    public static final String[] WHITE_LIST = {
            // 测试接口
            "/test/test1",
            "/test/test2",

            // 后端登录接口
            // 账号密码登录
            "/user/login",
            "/user/mini/login",

            // 后端注册接口
            "/user/register",

            // swagger相关
            "/favicon.ico",
            "/swagger-ui.html",
            "/doc.html",
            "/webjars/**",
            "/swagger-resources/**",
            "/v2/**",
            "/configuration/ui",
            "/configuration/security",
            "/tool/forget/password",
            "/tool/sms",
            "/user/sms/login",
            "/goods/batchExport",

            // 小程序相关
            "/mini/login",
    };
}
