package com.xie.springbootinit.base.respose;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.xie.springbootinit.base.resultcode.BaseCode;
import com.xie.springbootinit.base.resultcode.StatusCode;
import lombok.Data;

/**
 * @Author: Xrx
 * @Description: 返回格式基础类
 * @CreateTime: 2023/5/14 10:13
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse<T> {

    private int code;
    private String message;
    private T data;

    public BaseResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public BaseResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public BaseResponse(StatusCode code, T data) {
        this.code = code.getCode();
        this.message = code.getMessage();
        this.data = data;
    }

    public BaseResponse(StatusCode code) {
        this(code, null);
    }

    public BaseResponse(T data) {
        this.code = BaseCode.OK.getCode();
        this.message = BaseCode.OK.getMessage();
        this.data = data;
    }

    /**
     * 失败返回函数
     */
    public static BaseResponse fail(StatusCode statusCode) {
        return new BaseResponse<>(statusCode);
    }

    /**
     * 失败返回函数
     */
    public static BaseResponse fail(String message) {
        return new BaseResponse<>(message);
    }

    /**
     * 成功返回函数 (无数据)
     */
    public static BaseResponse ok() {
        BaseCode baseCode = BaseCode.OK;
        return new BaseResponse<>(baseCode.getCode(), baseCode.getMessage());
    }

    /**
     * 成功返回函数 (带数据)
     */
    public static <T> BaseResponse<T> ok(T data) {
        BaseCode baseCode = BaseCode.OK;
        return new BaseResponse<>(baseCode.getCode(), baseCode.getMessage(), data);
    }

    /**
     * 成功返回函数 (带数据)
     */
    public static <T> BaseResponse<T> ok(T data, String message) {
        return new BaseResponse<>(BaseCode.OK.getCode(), message, data);
    }

}
