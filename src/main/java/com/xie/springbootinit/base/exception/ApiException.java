package com.xie.springbootinit.base.exception;

import com.xie.springbootinit.base.resultcode.BaseCode;
import com.xie.springbootinit.base.resultcode.StatusCode;
import lombok.Getter;

/**
 * 自定义异常类
 *
 */
@Getter
public class ApiException extends RuntimeException {

    /**
     * 状态码
     */
    private int code;

    /**
     * 状态码配套的msg
     */
    private String msg;

    // 手动设置异常
    public ApiException(StatusCode statusCode, String message) {
        super(message);
        this.code = statusCode.getCode();
        this.msg = statusCode.getMessage();
    }

    public ApiException(StatusCode statusCode) {
        super(statusCode.getMessage());
        this.code = statusCode.getCode();
        this.msg = statusCode.getMessage();
    }

    // 默认使用INNER_ERROR的状态码
    public ApiException(String message) {
        super(message);
        this.code = BaseCode.InnerError.getCode();
        this.msg = BaseCode.InnerError.getMessage();
    }

}
