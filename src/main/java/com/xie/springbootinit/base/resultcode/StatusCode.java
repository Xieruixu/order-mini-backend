package com.xie.springbootinit.base.resultcode;

/**
 * @Author: Xrx
 * @Description: 通用返回接口
 * @CreateTime: 2023/5/14 10:15
 */
public interface StatusCode {

    int getCode();

    String getMessage();

}
