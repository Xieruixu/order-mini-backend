package com.xie.springbootinit.base.resultcode;

/**
 * @Author: Xrx
 * @Description:
 * @CreateTime: 2023/5/14 10:32
 */
public enum LoginCode implements StatusCode {

    /**
     * 用户令牌失效
     */
    ACCESS_TOKEN_ERROR(400, "用户令牌失效"),

    /**
     * 用户未登录
     */
    UN_LOGIN_ERROR(401, "用户未登录"),

    /**
     * 用户名或密码错误
     */
    PWD_ERROR(402, "用户名或密码错误"),

    /**
     * 账户已被禁用
     */
    BAN_ERROR(403, "账户已被禁用");


    private int code;
    private String message;

    LoginCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
