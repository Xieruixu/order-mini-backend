package com.xie.springbootinit.base.resultcode;

/**
 * @Author: Xrx
 * @Description: 自定义返回枚举类
 * @CreateTime: 2023/5/14 10:17
 */
@SuppressWarnings(value = "all")
public enum BaseCode implements StatusCode {

    OK(200, "成功"),

    InnerError(500, "系统错误"),

    ParamterValueError(501, "参数校验错误"),

    AccessDenied(502, "系统没有访问权限"),

    ResponsePackError(503, "封装返回请求错误");

    private int code;
    private String message;

    BaseCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
