package com.xie.springbootinit.base.utils;


import com.xie.springbootinit.model.entity.SysUser;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @Author Xrx
 * @Description 获取当前登录用户的基本信息
 * @CreateTime: 2023/2/15 11:24
 */
public class SecurityUtils {

    /**
     * 从Security主体信息中获取用户信息
     * @return
     */
    public static SysUser getUser() {
        SysUser user = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        user.setPassword(null);
        user.setUserName(user.getUsername());
        return user;
    }

    /**
     * 在security中获取用户名
     * @return
     */
    public static String getUsername() {
        return getUser().getUsername();
    }

    /**
     * 在security中获取用户ID
     * @return
     */
    public static Long getUserId() {
        return getUser().getId();
    }

    ///**
    // * 在security中获取用户小程序ID
    // * @return
    // */
    //public static String getOpenId() {
    //    return getUser().getMyOpenId();
    //}

}
