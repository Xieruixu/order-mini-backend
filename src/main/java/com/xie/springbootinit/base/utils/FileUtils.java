package com.xie.springbootinit.base.utils;


import com.xie.springbootinit.base.exception.ApiException;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * @Author: Xrx
 * @Description:
 * @CreateTime: 2023/7/3 11:08
 */
@Slf4j
public class FileUtils {

    /**
     * 读取文件
     * @param response
     * @param filePath
     */
    public static void readFile(HttpServletResponse response, String filePath) {
        if (!StringTools.pathIsOk(filePath)) {
            log.error("文件路径不正确");
            return;
        }
        OutputStream out = null;
        FileInputStream in = null;
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                log.error("文件不存在");
                return;
            }
            in = new FileInputStream(file);
            byte[] byteData = new byte[1024];
            int len;
            out = response.getOutputStream();
            while ((len = in.read(byteData)) != -1) {
                out.write(byteData, 0, len);
            }
            out.flush();
        } catch (FileNotFoundException e) {
            log.error("文件不存在");
            throw new ApiException("文件不存在");
        } catch (IOException e) {
            log.error("文件读取异常");
            throw new ApiException("文件读取异常");
        } finally {
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                    log.error("文件输出流关闭异常");
                    throw new ApiException("文件输出流关闭异常");
                }
            }
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.error("文件输入流关闭异常");
                    throw new ApiException("文件输入流关闭异常");
                }
            }
        }


    }




















}
