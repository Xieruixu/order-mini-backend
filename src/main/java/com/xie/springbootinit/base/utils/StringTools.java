package com.xie.springbootinit.base.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;

/**
 * @Author: Xrx
 * @Description:
 * @CreateTime: 2023/6/30 17:49
 */
public class StringTools {

    /**
     * 生成随机数字符串
     * @param count
     * @return
     */
    public static final String getRandomNumber(Integer count) {
        return RandomStringUtils.random(count, false, true);
    }

    public static boolean isEmpty(String str) {
        return null == str || "".equals(str.trim()) || "null".equals(str.trim()) || "\u0000".equals(str.trim());
    }


    public static boolean pathIsOk(String filePath) {
        if (isEmpty(filePath)) {
            return false;
        }
        File file = new File(filePath);
        if (!file.exists()) {
            return false;
        }
        return true;
    }
}
