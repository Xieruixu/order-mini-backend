package com.xie.springbootinit.base.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Author Xrx
 * @Description
 * @CreateTime: 2023/2/18 16:53
 */
@Component
@Slf4j
@SuppressWarnings(value = { "unchecked", "rawtypes" })
public class RedisUtils {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 向redis中存入值
     * @param key 键
     * @param value 值
     */
    public boolean setValue(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            log.error("向redis中存入值时异常-->{}", e.getMessage());
            return false;
        }
    }

    /**
     * 向redis中存值并指定过期时间
     * SECONDS: 秒
     * MINUTES: 分
     * HOURS: 时
     * DAYS: 天
     */
    public boolean setValueTime(String key, Object value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.MINUTES);
            } else {
                setValue(key, value);
            }
            return true;
        } catch (Exception e) {
            log.error("设置缓存并指定过期时间异常-->{}", e.getMessage());
            return false;
        }
    }

    /**
     * 根据key获取redis的值
     * @param key
     * @return
     */
    public Object getValue(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }


    /**
     * 根据key删除redis中的缓存
     * @param keys
     */
    public void delKey(String... keys) {
        if (keys != null && keys.length > 0) {
            if (keys.length == 1) {
                redisTemplate.delete(keys[0]);
            } else {
                for (String key : keys) {
                    redisTemplate.delete(key);
                }
            }
        }
    }

    /**
     * 判断值是否存在
     * @param key
     * @return
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            log.error("redis中值不存在-->{}", e.getMessage());
            return false;
        }
    }

    /**
     * 获取redis键的过期时间
     * 0代表永久有效
     * 大于0就剩多少分钟失效
     * @param key
     */
    public Long isExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.MINUTES);
    }

    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.MINUTES);
            }
            return true;
        } catch (Exception e) {
            log.error("给旧的缓存设置新的过期时间异常--> {}", e.getMessage());
            return false;
        }
    }















}
