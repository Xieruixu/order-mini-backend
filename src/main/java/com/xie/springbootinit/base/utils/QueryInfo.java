package com.xie.springbootinit.base.utils;

import lombok.Data;

/**
 * @Author Xrx
 * @Description
 * @CreateTime: 2023/2/12 15:54
 */
@Data
public class QueryInfo {
    /**
PageRequest pageRequest 第几页
     */
    private Integer pageNumber;

    /**
     * 一页多少条数据
     */
    private Integer pageSize;

    /**
     * 查询的内容
     */
    private String queryString;
}
