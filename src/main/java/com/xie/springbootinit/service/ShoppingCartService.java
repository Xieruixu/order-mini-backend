package com.xie.springbootinit.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xie.springbootinit.model.entity.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {

    Object add(ShoppingCart shoppingCart);

}
