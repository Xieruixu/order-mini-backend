package com.xie.springbootinit.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xie.springbootinit.model.entity.Employee;

public interface EmployeeService extends IService<Employee> {
}
