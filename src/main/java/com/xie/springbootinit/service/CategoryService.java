package com.xie.springbootinit.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xie.springbootinit.model.entity.Category;

public interface CategoryService extends IService<Category> {

    void remove(Long id);

}
