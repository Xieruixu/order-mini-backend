package com.xie.springbootinit.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xie.springbootinit.model.entity.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {

}
