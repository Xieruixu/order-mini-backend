package com.xie.springbootinit.service.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xie.springbootinit.base.exception.ApiException;
import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.base.resultcode.BaseCode;
import com.xie.springbootinit.base.resultcode.LoginCode;
import com.xie.springbootinit.base.utils.*;
import com.xie.springbootinit.config.security.service.UserDetailsServiceImpl;
import com.xie.springbootinit.constant.GlobalConstant;
import com.xie.springbootinit.constant.UserConstant;
import com.xie.springbootinit.mapper.UserMapper;
import com.xie.springbootinit.model.dto.user.UserLoginRequest;
import com.xie.springbootinit.model.entity.SysUser;
import com.xie.springbootinit.model.vo.user.UserVo;
import com.xie.springbootinit.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @Author Xrx
 * @Description
 * @CreateTime: 2023/2/12 16:25
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, SysUser> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private RedisUtils redisUtils;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Value("${user.properties.appid}")
    private String appid;

    @Value("${user.properties.secret}")
    private String secret;

    /**
     * 获取当前用户信息
     * @param user 查找的用户信息
     * @return 用户vo
     */
    @Override
    public UserVo getUserInfo(SysUser user) {
        return userMapper.getUserInfo(user);
    }

    /**
     * 用户注册
     * @param userName 用户名
     * @param userPassword 用户密码
     * @param checkPassword 确认密码
     * @return 用户id
     */
    @Override
    public long userRegister(String userName, String userPassword, String checkPassword) {
        // 1.校验
        if (StringUtils.isAllBlank(userName, userPassword, checkPassword)) {
            throw new ApiException(BaseCode.ParamterValueError, "参数为空");
        }
        if (userName.length() < 4) {
            throw new ApiException(BaseCode.ParamterValueError, "用户名长度过短");
        }
        if (userPassword.length() < 8 || checkPassword.length() < 8) {
            throw new ApiException(BaseCode.ParamterValueError, "用户密码过短");
        }
        if (!userPassword.equals(checkPassword)) {
            throw new ApiException(BaseCode.ParamterValueError, "两次输入的密码不一致");
        }
        synchronized (userName.intern()) {
            // 用户名不能重复
            QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_name", userName);
            Long count = this.baseMapper.selectCount(queryWrapper);
            ThrowUtils.throwIf(count > 0, BaseCode.ParamterValueError, "账号已存在");
            // 2.加密
            String encryptPwd = passwordEncoder.encode(MD5Utils.md5(userPassword + GlobalConstant.SALT));
            // 3.插入数据
            SysUser user = new SysUser();
            user.setUserName(userName);
            user.setPassword(encryptPwd);
            boolean save = this.save(user);
            ThrowUtils.throwIf(!save, BaseCode.InnerError, "注册失败");
            return user.getId();
        }
    }

    /**
     * 登录接口
     * @param userLoginRequest 登录参数： 账号和密码
     * @return
     */
    @Override
    public BaseResponse userLogin(UserLoginRequest userLoginRequest) {
        UserDetails userDetails;
        if (!StringUtils.isNotEmpty(userLoginRequest.getUserName()) || !StringUtils.isNotEmpty(userLoginRequest.getPassword())) {
            throw new ApiException(BaseCode.ParamterValueError, "用户名或密码为空");
        }
        log.info("1. 开始登录");
        userDetails = userDetailsService.loadUserByUsername(userLoginRequest.getUserName());
        log.info("2. 判断用户是否存在，密码是否正确");
        if (Objects.isNull(userDetails)
                || !passwordEncoder.matches(MD5Utils.md5(userLoginRequest.getPassword() + GlobalConstant.SALT), userDetails.getPassword())) {
            throw new ApiException(BaseCode.ParamterValueError, "账号或密码错误，请重新输入！");
        }
        if (!userDetails.isEnabled()) {
            throw new ApiException(LoginCode.BAN_ERROR);
        }
        log.info("登录成功，在security对象中存入登陆者信息");
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        log.info("根据登录信息获取token");
        //需要借助jwt来生成token
        String token = tokenUtils.generateToken((SysUser) userDetails);
        Map<String, String> map = new HashMap<>(2);
        map.put("tokenHead", tokenHead);
        map.put("token", token);
        return BaseResponse.ok(map, "登录成功！");
    }

    /**
     * 微信小程序登录
     * @param code 登录参数
     * @return
     */
    @Override
    public BaseResponse miniLogin(String code) {
        if (StringUtils.isBlank(code)) {
            throw new ApiException(BaseCode.ParamterValueError);
        }
        log.info("code: {}", code);
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid=" + appid
                + "&secret=" + secret + "&js_code=" + code + "&grant_type=authorization_code";
        // 发送请求
        log.info("url: {}", url);
        String result = HttpUtils.getResponse(url);
        log.info("result: {}", result);
        JSONObject jsonObject = JSON.parseObject(result);
        log.info("jsonobject: {}", jsonObject);
        String openid = jsonObject.getString("openid");
        String sessionKey = jsonObject.getString("session_key");
        log.info("微信小程序唯一标识: {}", openid);
        log.info("微信小程序会话密钥: {}", sessionKey);
        UserDetails userDetails = userDetailsService.loadUserByUsername(openid);
        if (Objects.isNull(userDetails)) {
            SysUser newUser = new SysUser();
            newUser.setUserName(openid);
            newUser.setOpenId(openid);
            this.save(newUser);
            newUser = baseMapper.selectOne(new QueryWrapper<SysUser>().eq("user_name", openid));
            log.info("newUser: {}", newUser);
            redisUtils.setValueTime(UserConstant.USER_INFO + newUser.getId(), newUser, 60);
            userDetails = newUser;
        }
        if (!userDetails.isEnabled()) {
            throw new ApiException(LoginCode.BAN_ERROR);
        }
        log.info("微信小程序登录成功，在security对象中存入登陆者信息");
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        log.info("根据登录信息获取token");
        //需要借助jwt来生成token
        String token = tokenUtils.generateToken((SysUser) userDetails);
        Map<String, Object> map = new HashMap<>();
        map.put("tokenHead", tokenHead);
        map.put("token", token);
        map.put("userInfo", userDetails);
        map.put("sessionKey", sessionKey);
        return BaseResponse.ok(map, "登录成功！");
    }

    @Override
    public SysUser findByUsername(String username) {
        if (Objects.isNull(username)) {
            return null;
        }
        return userMapper.findByUsername(username);
    }

}
