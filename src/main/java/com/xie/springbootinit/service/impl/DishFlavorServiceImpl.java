package com.xie.springbootinit.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xie.springbootinit.mapper.DishFlavorMapper;
import com.xie.springbootinit.model.entity.DishFlavor;
import com.xie.springbootinit.service.DishFlavorService;
import org.springframework.stereotype.Service;

@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {
}
