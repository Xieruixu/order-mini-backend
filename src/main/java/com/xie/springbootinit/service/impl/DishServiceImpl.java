package com.xie.springbootinit.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xie.springbootinit.base.exception.ApiException;
import com.xie.springbootinit.base.utils.SecurityUtils;
import com.xie.springbootinit.constant.FileConstant;
import com.xie.springbootinit.mapper.DishMapper;
import com.xie.springbootinit.model.entity.Category;
import com.xie.springbootinit.model.entity.Dish;
import com.xie.springbootinit.model.entity.DishFlavor;
import com.xie.springbootinit.model.vo.dish.DishAddVo;
import com.xie.springbootinit.service.CategoryService;
import com.xie.springbootinit.service.DishFlavorService;
import com.xie.springbootinit.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {

    @Autowired
    private DishFlavorService dishFlavorService;

    @Autowired
    private CategoryService categoryService;

    @Value("${project.file.dish}")
    private String dishFile;

    /**
     * 新增菜品，同时保存对应的口味数据
     * @param dishAddVo
     */
    @Transactional(rollbackFor = Exception.class)
    public Dish saveWithFlavor(DishAddVo dishAddVo) {
        Long userId = SecurityUtils.getUserId();
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishAddVo, dish);
        // 设置菜品的类别
        LambdaQueryWrapper<Category> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Category::getName, dishAddVo.getCategoryName());
        wrapper.eq(Category::getType, 1);
        Category category = categoryService.getBaseMapper().selectOne(wrapper);
        if (Objects.isNull(category)) {
            throw new ApiException("菜品类别不存在");
        }
        dish.setCategoryId(category.getId());
        // 保存菜品的基本信息到菜品表dish
        this.save(dish);
        // 获取菜品的id
        Long dishId = baseMapper.selectCount(new LambdaQueryWrapper<Dish>().eq(Dish::getName, dish.getName()));
        // 设置菜品的口味
        List<String> dishFlavors = dishAddVo.getDishFlavors();
        List<DishFlavor> flavorList = dishFlavors.stream().map((item) -> {
            DishFlavor dishFlavor = new DishFlavor();
            dishFlavor.setDishId(dishId);
            dishFlavor.setName(item);
            return dishFlavor;
        }).collect(Collectors.toList());
        dishFlavorService.saveBatch(flavorList);
        // 设置菜品的图片路径
        if (!Objects.isNull(dishAddVo.getImage())) {
            this.updateDishImg(userId, dishId, dishAddVo.getImage());
        } else {
            // 设置默认图片
            dish.setImageUrl(dishFile + FileConstant.DEFAULT_DISH_IMG);
        }
        // 保存菜品的基本信息到菜品表dish
        this.save(dish);
        log.info("新增菜品成功，菜品id为：{}", dishId);
        return baseMapper.selectOne(new LambdaQueryWrapper<Dish>().eq(Dish::getId, dishId));
    }

    /**
     * 根据id查询菜品信息和对应的口味信息
     * @param id
     * @return
     */
    public DishAddVo getByIdWithFlavor(Long id) {
        //查询菜品基本信息，从dish表查询
        Dish dish = this.getById(id);

        DishAddVo dishAddVo = new DishAddVo();
        BeanUtils.copyProperties(dish, dishAddVo);

        //查询当前菜品对应的口味信息，从dish_flavor表查询
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,dish.getId());
        List<DishFlavor> flavors = dishFlavorService.list(queryWrapper);
        dishAddVo.setFlavors(flavors);

        return dishAddVo;
    }

    @Override
    @Transactional
    public void updateWithFlavor(DishAddVo dishAddVo) {
        //更新dish表基本信息
        this.updateById(dishAddVo);

        //清理当前菜品对应口味数据---dish_flavor表的delete操作
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(DishFlavor::getDishId, dishAddVo.getId());

        dishFlavorService.remove(queryWrapper);

        //添加当前提交过来的口味数据---dish_flavor表的insert操作
        List<DishFlavor> flavors = dishAddVo.getFlavors();

        flavors = flavors.stream().map((item) -> {
            item.setDishId(dishAddVo.getId());
            return item;
        }).collect(Collectors.toList());

        dishFlavorService.saveBatch(flavors);
    }

    private void updateDishImg(Long userId, Long dishId, MultipartFile avatar) {
        File folder = new File(dishFile);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        String dishPath = userId + FileConstant.IMG_SUFFIX;
        File file = new File(dishPath);
        try {
            avatar.transferTo(file);
        } catch (Exception e) {
            log.error("上传菜品图片失败", e);
            throw new ApiException("上传菜品图片失败");
        }
        Dish dish = baseMapper.selectOne(new LambdaQueryWrapper<Dish>().eq(Dish::getId, dishId));
        dish.setImageUrl(dishPath);
        baseMapper.updateById(dish);
        log.info("上传菜品图片成功");
    }
}
