package com.xie.springbootinit.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xie.springbootinit.mapper.SetmealDishMapper;
import com.xie.springbootinit.model.entity.SetMealDish;
import com.xie.springbootinit.service.SetmealDishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetMealDish> implements SetmealDishService {
}
