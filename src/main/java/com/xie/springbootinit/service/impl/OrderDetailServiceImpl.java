package com.xie.springbootinit.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xie.springbootinit.mapper.OrderDetailMapper;
import com.xie.springbootinit.model.entity.OrderDetail;
import com.xie.springbootinit.service.OrderDetailService;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {

}