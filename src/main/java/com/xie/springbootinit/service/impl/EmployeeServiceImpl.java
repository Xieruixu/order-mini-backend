package com.xie.springbootinit.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xie.springbootinit.mapper.EmployeeMapper;
import com.xie.springbootinit.model.entity.Employee;
import com.xie.springbootinit.service.EmployeeService;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {
}
