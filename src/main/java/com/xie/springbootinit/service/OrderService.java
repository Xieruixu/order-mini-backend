package com.xie.springbootinit.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xie.springbootinit.model.entity.Orders;
import com.xie.springbootinit.model.vo.order.OrderVo;

public interface OrderService extends IService<Orders> {

    /**
     * 用户下单
     * @param ordersVo
     */
    public void submit(OrderVo ordersVo);
}
