package com.xie.springbootinit.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xie.springbootinit.model.entity.SetMealDish;

public interface SetmealDishService extends IService<SetMealDish> {
}
