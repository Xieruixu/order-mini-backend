package com.xie.springbootinit.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xie.springbootinit.model.entity.Dish;
import com.xie.springbootinit.model.vo.dish.DishAddVo;

public interface DishService extends IService<Dish> {

    /**
     * 新增菜品，同时插入菜品对应的口味数据，需要操作两张表：dish、dish_flavor
     * @param dishAddVo
     */
    Dish saveWithFlavor(DishAddVo dishAddVo);

    /**
     * 根据id查询菜品信息和对应的口味信息
     * @param id
     * @return
     */
    DishAddVo getByIdWithFlavor(Long id);

    /**
     * 更新菜品信息，同时更新对应的口味信息
     * @param dishAddVo
     */
    void updateWithFlavor(DishAddVo dishAddVo);
}
