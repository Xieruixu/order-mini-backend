package com.xie.springbootinit.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.model.dto.user.UserLoginRequest;
import com.xie.springbootinit.model.entity.SysUser;
import com.xie.springbootinit.model.vo.user.UserVo;

/**
 * @Author Xrx
 * @Description 用户操作服务接口
 * @CreateTime: 2023/2/12 16:24
 */
public interface UserService extends IService<SysUser> {

    /**
     * 用户注册
     * @param userName 用户名
     * @param userPassword 密码
     * @param checkPassword 确认密码
     * @return 用户id
     */
    long userRegister(String userName, String userPassword, String checkPassword);

    /**
     * 登录接口
     * @param userLoginRequest 登录参数： 账号和密码
     * @return
     */
    BaseResponse userLogin(UserLoginRequest userLoginRequest);

    /**
     * 获取用户信息
     * @param user
     * @return
     */
    UserVo getUserInfo(SysUser user);

    /**
     * 微信登录接口
     * @param code 登录参数： code
     */
    BaseResponse miniLogin(String code);

    /**
     * 根据用户名获取用户信息
     * @param username
     * @return
     */
    SysUser findByUsername(String username);


}
