package com.xie.springbootinit.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xie.springbootinit.model.entity.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
