package com.xie.springbootinit.controller.cart;

import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.base.resultcode.BaseCode;
import com.xie.springbootinit.base.utils.RedisUtils;
import com.xie.springbootinit.base.utils.SecurityUtils;
import com.xie.springbootinit.constant.ShoppingCartConstant;
import com.xie.springbootinit.model.vo.cart.CartVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;


/**
 * 购物车
 */
@Slf4j
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 添加购物车
     * @param cartVo
     * @return
     */
    @PostMapping("/add")
    public Object add(@RequestBody CartVo cartVo) {
        log.info("购物车中添加商品,shoppingCartVo:{}", cartVo);
        if (Objects.isNull(cartVo)) {
            return BaseResponse.fail(BaseCode.ParamterValueError);
        }
        Long userId = SecurityUtils.getUserId();
        redisUtils.setValue(ShoppingCartConstant.SHOPPING_CART_PREFIX + userId, cartVo);
        return BaseResponse.ok("添加购物车成功");
    }

    /**
     * 查看购物车
     * @return
     */
    @GetMapping("/list")
    public Object list() {
        Long userId = SecurityUtils.getUserId();
        log.info("查看购物车,userId:{}", userId);
        CartVo cart = (CartVo) redisUtils.getValue(ShoppingCartConstant.SHOPPING_CART_PREFIX + userId);
        log.info("查看购物车成功,cart:{}", cart);
        return BaseResponse.ok(cart);
    }

    /**
     * 更新购物车中的菜品
     */
    @PostMapping("/update")
    public Object update(@RequestBody CartVo cartVo) {
        log.info("更新购物车中的菜品,cartVo:{}", cartVo);
        if (Objects.isNull(cartVo)) {
            return BaseResponse.fail(BaseCode.ParamterValueError);
        }
        Long userId = SecurityUtils.getUserId();
        redisUtils.setValue(ShoppingCartConstant.SHOPPING_CART_PREFIX + userId, cartVo);
        return BaseResponse.ok("更新购物车成功");
    }

    /**
     * 清空购物车
     * @return
     */
    @DeleteMapping("/clean")
    public Object clean() {
        Long userId = SecurityUtils.getUserId();
        log.info("清空购物车,userId:{}", userId);
        redisUtils.delKey(ShoppingCartConstant.SHOPPING_CART_PREFIX + userId);
        return BaseResponse.ok("清空购物车成功");
    }
}