package com.xie.springbootinit.controller.login;


import com.xie.springbootinit.base.exception.ApiException;
import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.base.resultcode.BaseCode;
import com.xie.springbootinit.base.utils.RedisUtils;
import com.xie.springbootinit.base.utils.SecurityUtils;
import com.xie.springbootinit.constant.UserConstant;
import com.xie.springbootinit.model.dto.user.Code;
import com.xie.springbootinit.model.dto.user.UserLoginRequest;
import com.xie.springbootinit.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @Author: Xrx
 * @Description: 用户登录类
 * @CreateTime: 2023/5/13 10:24
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserLoginController {

    private UserService userService;

    private RedisUtils redisUtils;

    @Autowired
    public UserLoginController(UserService userService, RedisUtils redisUtils) {
        this.userService = userService;
        this.redisUtils = redisUtils;
    }

    /**
     * 用户登录(账号密码)
     * @param userLoginRequest
     * @return
     */
    @PostMapping("/login")
    public BaseResponse userLogin(@RequestBody UserLoginRequest userLoginRequest) {
        if (userLoginRequest == null) {
            throw new ApiException(BaseCode.ParamterValueError, "登录参数异常");
        }
        return userService.userLogin(userLoginRequest);
    }

    /**
     * 用户登录(微信小程序)
     * @param code
     * @return
     */
    @PostMapping("/mini/login")
    public BaseResponse miniLogin(@RequestBody Code code) {
        String js_code = code.getCode();
        if (StringUtils.isBlank(js_code)) {
            throw new ApiException(BaseCode.ParamterValueError, "登录参数异常");
        }
        return userService.miniLogin(js_code);
    }

    /**
     * 用户注销
     * @param request
     * @return
     */
    @PostMapping("/logout")
    public BaseResponse userLogout(HttpServletRequest request) {
        if (Objects.isNull(request)) {
            throw new ApiException(BaseCode.InnerError, "链接异常");
        }
        redisUtils.delKey(UserConstant.USER_INFO + SecurityUtils.getUsername());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (Objects.isNull(authentication)) {
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return BaseResponse.ok("退出成功");
    }




}
