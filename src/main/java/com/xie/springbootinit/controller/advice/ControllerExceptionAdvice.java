package com.xie.springbootinit.controller.advice;

import com.xie.springbootinit.base.exception.ApiException;
import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.base.resultcode.BaseCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author: Xrx
 * @Description:
 * @CreateTime: 2023/5/14 16:18
 */
@Slf4j
@RestControllerAdvice
public class ControllerExceptionAdvice {

    @ExceptionHandler({BindException.class})
    public BaseResponse MethodArgumentNotValidExceptionHandler(BindException e) {
        // 从异常对象中拿到ObjectError对象
        ObjectError objectError = e.getBindingResult().getAllErrors().get(0);
        FieldError fieldError = (FieldError) objectError;
        return new BaseResponse(BaseCode.ParamterValueError, fieldError.getField() + " " + objectError.getDefaultMessage());
    }

    @ExceptionHandler(ApiException.class)
    public BaseResponse ApiException(ApiException e) {
        log.error(e.getMessage(), e);
        return new BaseResponse(e.getCode(), e.getMsg(), e.getMessage());
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public BaseResponse ApiException(UsernameNotFoundException e) {
        log.error(e.getMessage(), e);
        return new BaseResponse(BaseCode.InnerError.getCode(), e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public BaseResponse ApiException(Exception e) {
        log.error(e.getMessage(), e);
        return new BaseResponse(BaseCode.InnerError.getCode(), BaseCode.InnerError.getMessage(), e.getMessage());
    }
}
