package com.xie.springbootinit.controller.advice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xie.springbootinit.base.exception.ApiException;
import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.base.resultcode.BaseCode;
import com.xie.springbootinit.controller.advice.annotation.NotControllerResponseAdvice;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * @Author: Xrx
 * @Description: 自定义返回格式化对象类
 * @CreateTime: 2023/5/14 16:26
 */
@RestControllerAdvice(basePackages = {"com.xie.springbootinit.controller", "com.xie.springbootinit.config.security.handler"})
public class ControllerResponseAdvice implements ResponseBodyAdvice<Object> {


    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        // response是BaseResponse类型,或者注释了NotControllerResponseAdvice都不进行包装
        return !(returnType.getParameterType().isAssignableFrom(BaseResponse.class)
                || returnType.hasMethodAnnotation(NotControllerResponseAdvice.class));
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        // String类型不能直接包装
        if (returnType.getParameterType().equals(String.class)) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                // 将数据包装在BaseResponse里后转为json串进行返回
                return objectMapper.writeValueAsString(new BaseResponse<>(body.toString()));
            } catch (JsonProcessingException e) {
                throw new ApiException(BaseCode.ResponsePackError, e.getMessage());
            }
        }
        // 否则直接包装成BaseResponse返回
        return new BaseResponse(body);
    }
}
