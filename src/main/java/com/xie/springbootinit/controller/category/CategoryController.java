package com.xie.springbootinit.controller.category;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.model.vo.category.CategoryVo;
import com.xie.springbootinit.model.vo.common.PageRequest;
import com.xie.springbootinit.model.entity.Category;
import com.xie.springbootinit.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 分类管理
 */
@RestController
@RequestMapping("/category")
@Slf4j
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 新增分类
     * @param categoryVo
     * @return
     */
    @PostMapping("/save")
    public Object save(@RequestBody CategoryVo categoryVo) {
        log.info("category:{}", categoryVo);
        Category category = new Category();
        BeanUtils.copyProperties(categoryVo, category);
        categoryService.save(category);
        return BaseResponse.ok("新增分类成功");
    }


    /**
     * 根据id删除分类
     * @param categoryVo
     * @return
     */
    @DeleteMapping("/delete")
    public Object delete(@RequestBody CategoryVo categoryVo) {
        log.info("删除分类信息：{}", categoryVo);
        LambdaQueryWrapper<Category> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Category::getType, categoryVo.getType());
        wrapper.eq(Category::getName, categoryVo.getName());
        categoryService.remove(wrapper);

        return BaseResponse.ok("分类信息删除成功");
    }

    /**
     * 根据id修改分类信息
     * @param categoryVo
     * @return
     */
    @PutMapping("/update")
    public Object update(@RequestBody CategoryVo categoryVo) {
        log.info("修改分类信息：{}", categoryVo);
        LambdaQueryWrapper<Category> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Category::getType, categoryVo.getType());
        wrapper.eq(Category::getName, categoryVo.getName());
        categoryService.update(wrapper);
        return BaseResponse.ok("修改分类信息成功");
    }

    /**
     * 分页查询
     * @param pageRequest
     * @return
     */
    @GetMapping("/page")
    public Object page(@RequestBody PageRequest pageRequest) {
        //分页构造器
        Page<Category> pageInfo = new Page<>(pageRequest.getCurrent(), pageRequest.getPageSize());
        //条件构造器
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(pageRequest.getData() != null, Category::getType, pageRequest.getData());
        queryWrapper.orderByDesc(Category::getUpdatedTime);
        //分页查询
        Page<Category> page = categoryService.page(pageInfo, queryWrapper);
        return BaseResponse.ok(page);
    }

    /**
     * 根据条件查询分类数据
     * @param pageRequest
     * @return
     */
    @GetMapping("/list")
    public Object list(@RequestBody PageRequest pageRequest) {
        //条件构造器
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        //添加条件
        queryWrapper.eq(pageRequest.getData() != null, Category::getType, pageRequest.getData());
        queryWrapper.orderByDesc(Category::getUpdatedTime);
        List<Category> list = categoryService.list(queryWrapper);
        return BaseResponse.ok(list);
    }
}
