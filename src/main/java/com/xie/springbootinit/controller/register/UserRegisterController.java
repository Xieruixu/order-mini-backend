package com.xie.springbootinit.controller.register;

import com.xie.springbootinit.base.exception.ApiException;
import com.xie.springbootinit.base.resultcode.BaseCode;
import com.xie.springbootinit.model.dto.user.UserRegisterRequest;
import com.xie.springbootinit.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * @Author: Xrx
 * @Description: 用户注册类
 * @CreateTime: 2023/5/13 10:36
 */
@RestController
@RequestMapping("/user")
public class UserRegisterController {

    private UserService userService;

    @Autowired
    public UserRegisterController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 用户注册
     * @param userRegisterRequest
     * @return
     */
    @PostMapping("/register")
    public Object userRegister(@RequestBody UserRegisterRequest userRegisterRequest) {
        if (Objects.isNull(userRegisterRequest)) {
            throw new ApiException(BaseCode.ParamterValueError);
        }
        String userName = userRegisterRequest.getUserName();
        String userPassword = userRegisterRequest.getPassword();
        String checkPassword = userRegisterRequest.getCheckPassword();
        if (StringUtils.isAnyBlank(userName, userPassword, checkPassword)) {
            return null;
        }
        long userId = userService.userRegister(userName, userPassword, checkPassword);
        return userId;
    }

}
