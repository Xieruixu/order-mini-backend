package com.xie.springbootinit.controller.dish;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.model.dto.dish.SetMealDto;
import com.xie.springbootinit.model.entity.Category;
import com.xie.springbootinit.model.entity.SetMeal;
import com.xie.springbootinit.model.vo.common.PageRequest;
import com.xie.springbootinit.service.CategoryService;
import com.xie.springbootinit.service.SetmealDishService;
import com.xie.springbootinit.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 套餐管理
 */
@RestController
@RequestMapping("/setmeal")
@Slf4j
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SetmealDishService setmealDishService;

    /**
     * 新增套餐
     *
     * @param setmealDto
     * @return
     */
    @PostMapping
    @CacheEvict(value = "setmealCache", allEntries = true)
    public Object save(@RequestBody SetMealDto setmealDto) {
        log.info("套餐信息：{}", setmealDto);

        setmealService.saveWithDish(setmealDto);

        return BaseResponse.ok("新增套餐成功");
    }

    /**
     * 套餐分页查询
     * @param pageRequest
     * @return
     */
    @GetMapping("/page")
    public Object page(PageRequest pageRequest) {
        //分页构造器对象
        Page<SetMeal> pageInfo = new Page<>(pageRequest.getCurrent(), pageRequest.getPageSize());
        Page<SetMealDto> dtoPage = new Page<>();

        LambdaQueryWrapper<SetMeal> queryWrapper = new LambdaQueryWrapper<>();
        //添加查询条件，根据name进行like模糊查询
        queryWrapper.like(pageRequest.getData() != null, SetMeal::getName, pageRequest.getData());
        //添加排序条件，根据更新时间降序排列
        queryWrapper.orderByDesc(SetMeal::getUpdatedTime);

        setmealService.page(pageInfo, queryWrapper);

        //对象拷贝
        BeanUtils.copyProperties(pageInfo, dtoPage, "records");
        List<SetMeal> records = pageInfo.getRecords();

        List<SetMealDto> list = records.stream().map((item) -> {
            SetMealDto setmealDto = new SetMealDto();
            //对象拷贝
            BeanUtils.copyProperties(item, setmealDto);
            //分类id
            Long categoryId = item.getCategoryId();
            //根据分类id查询分类对象
            Category category = categoryService.getById(categoryId);
            if (category != null) {
                //分类名称
                String categoryName = category.getName();
                setmealDto.setCategoryName(categoryName);
            }
            return setmealDto;
        }).collect(Collectors.toList());

        dtoPage.setRecords(list);
        return BaseResponse.ok(dtoPage);
    }

    /**
     * 删除套餐
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    @CacheEvict(value = "setmealCache", allEntries = true)
    public Object delete(@RequestParam List<Long> ids) {
        log.info("ids:{}", ids);

        setmealService.removeWithDish(ids);

        return BaseResponse.ok("套餐数据删除成功");
    }

    /**
     * 根据条件查询套餐数据
     *
     * @param setmeal
     * @return
     */
    @GetMapping("/list")
    @Cacheable(value = "setmealCache", key = "#setmeal.categoryId + '_' + #setmeal.status")
    public Object list(SetMeal setmeal) {
        LambdaQueryWrapper<SetMeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmeal.getCategoryId() != null, SetMeal::getCategoryId, setmeal.getCategoryId());
        queryWrapper.eq(setmeal.getStatus() != null, SetMeal::getStatus, setmeal.getStatus());
        queryWrapper.orderByDesc(SetMeal::getUpdatedTime);

        List<SetMeal> list = setmealService.list(queryWrapper);

        return BaseResponse.ok(list);
    }
}
