package com.xie.springbootinit.controller.dish;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.base.resultcode.BaseCode;
import com.xie.springbootinit.base.utils.RedisUtils;
import com.xie.springbootinit.constant.DishConstant;
import com.xie.springbootinit.model.entity.Category;
import com.xie.springbootinit.model.entity.Dish;
import com.xie.springbootinit.model.entity.DishFlavor;
import com.xie.springbootinit.model.vo.dish.DishAddVo;
import com.xie.springbootinit.model.vo.common.PageRequest;
import com.xie.springbootinit.model.vo.dish.DishQueryVo;
import com.xie.springbootinit.service.CategoryService;
import com.xie.springbootinit.service.DishFlavorService;
import com.xie.springbootinit.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 菜品管理
 */
@RestController
@RequestMapping("/dish")
@Slf4j
public class DishController {

    @Autowired
    private DishService dishService;

    @Autowired
    private DishFlavorService dishFlavorService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RedisUtils redisUtils;

    @Value("${shop.name}")
    private String shopName;


    /**
     * 新增菜品
     * @param dishAddVo
     * @return
     */
    @PostMapping("/add")
    public Object add(@Valid @RequestBody DishAddVo dishAddVo, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return BaseResponse.fail(BaseCode.ParamterValueError);
        }
        log.info("新增菜品，请求参数为：{}", dishAddVo);
        Dish dish = dishService.saveWithFlavor(dishAddVo);
        // 将新增的菜品信息存入redis中，方便后续查询
        List<Dish> dishList = JSON.parseArray((String) redisUtils.getValue(DishConstant.DISH_PREFIX + shopName), Dish.class);
        if (!Objects.isNull(dishList)) {
            dishList = new ArrayList<>();
        }
        dishList.add(dish);
        redisUtils.setValue(DishConstant.DISH_PREFIX + shopName, dishList);
        return BaseResponse.ok("新增菜品成功");
    }

    /**
     * 根据条件查询对应的菜品数据
     * @param dishQueryVo
     * @return
     */
    @GetMapping("/list")
    public Object list(DishQueryVo dishQueryVo) {
        List<Dish> dishList = null;
        // 查询redis中是否存在菜品信息
        dishList = () redisUtils.getValue(DishConstant.DISH_PREFIX + shopName);

        if (dishAddVoList != null) {
            //如果存在，直接返回，无需查询数据库
            return dishAddVoList;
        }

        //构造查询条件
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(dish.getCategoryId() != null, Dish::getCategoryId, dish.getCategoryId());
        //添加条件，查询状态为1（起售状态）的菜品
        queryWrapper.eq(Dish::getStatus, 1);

        //添加排序条件
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdatedTime);

        List<Dish> list = dishService.list(queryWrapper);

        dishAddVoList = list.stream().map((item) -> {
            DishAddVo dishAddVo = new DishAddVo();

            BeanUtils.copyProperties(item, dishAddVo);

            Long categoryId = item.getCategoryId();//分类id
            //根据id查询分类对象
            Category category = categoryService.getById(categoryId);

            if (category != null) {
                String categoryName = category.getName();
                dishAddVo.setCategoryName(categoryName);
            }

            //当前菜品的id
            Long dishId = item.getId();
            LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(DishFlavor::getDishId, dishId);
            //SQL:select * from dish_flavor where dish_id = ?
            List<DishFlavor> dishFlavorList = dishFlavorService.list(lambdaQueryWrapper);
            dishAddVo.setFlavors(dishFlavorList);
            return dishAddVo;
        }).collect(Collectors.toList());

        //如果不存在，需要查询数据库，将查询到的菜品数据缓存到Redis
        redisUtils.setValueTime(key, dishAddVoList, 60);

        return dishAddVoList;
    }

    /**
     * 菜品信息分页查询
     * @param pageRequest
     * @return
     */
    @PostMapping("/page")
    public Object page(@RequestBody PageRequest pageRequest) {
        //构造分页构造器对象
        Page<Dish> pageInfo = new Page<>(pageRequest.getCurrent(), pageRequest.getPageSize());
        Page<DishAddVo> dishVoPage = new Page<>();

        //条件构造器
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        //添加过滤条件
        queryWrapper.like(pageRequest.getData() != null, Dish::getName, pageRequest.getData());
        //添加排序条件
        queryWrapper.orderByDesc(Dish::getUpdatedTime);

        //执行分页查询
        dishService.page(pageInfo, queryWrapper);

        //对象拷贝
        BeanUtils.copyProperties(pageInfo, dishVoPage, "records");

        List<Dish> records = pageInfo.getRecords();

        List<DishAddVo> list = records.stream().map((item) -> {
            DishAddVo dishAddVo = new DishAddVo();

            BeanUtils.copyProperties(item, dishAddVo);

            Long categoryId = item.getCategoryId();//分类id
            //根据id查询分类对象
            Category category = categoryService.getById(categoryId);

            if (category != null) {
                String categoryName = category.getName();
                dishAddVo.setCategoryName(categoryName);
            }
            return dishAddVo;
        }).collect(Collectors.toList());

        dishVoPage.setRecords(list);

        return BaseResponse.ok(dishVoPage);
    }

    /**
     * 根据id查询菜品信息和对应的口味信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Object get(@PathVariable Long id) {

        DishAddVo dishAddVo = dishService.getByIdWithFlavor(id);

        return BaseResponse.ok(dishAddVo);
    }

    /**
     * 修改菜品
     * @param dishAddVo
     * @return
     */
    @PutMapping
    public Object update(@RequestBody DishAddVo dishAddVo) {
        log.info(dishAddVo.toString());

        dishService.updateWithFlavor(dishAddVo);

        //清理某个分类下面的菜品缓存数据
        String key = DishConstant.dish + dishAddVo.getCategoryId() + "_1";
        redisUtils.delKey(key);

        return BaseResponse.ok("修改菜品成功");
    }



}
