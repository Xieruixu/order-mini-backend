package com.xie.springbootinit.controller.user;

import com.xie.springbootinit.base.exception.ApiException;
import com.xie.springbootinit.base.utils.ThrowUtils;
import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.base.resultcode.BaseCode;
import com.xie.springbootinit.model.dto.user.UserDeleteRequest;
import com.xie.springbootinit.model.dto.user.UserUpdateRequest;
import com.xie.springbootinit.model.entity.SysUser;
import com.xie.springbootinit.model.vo.user.UserVo;
import com.xie.springbootinit.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: Xrx
 * @Description: 用户管理类
 * @CreateTime: 2023/5/13 10:24
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 删除用户(根据id逻辑删除)
     * @param userDeleteRequest
     * @return
     */
    @PostMapping("/delete")
    public Object deleteUser(@RequestBody UserDeleteRequest userDeleteRequest) {
        if (userDeleteRequest == null || userDeleteRequest.getId() == null) {
            throw new ApiException(BaseCode.ParamterValueError);
        }
        boolean b = userService.removeById(userDeleteRequest.getId());
        return BaseResponse.ok(b);
    }

    /**
     * 更新用户
     * @param userUpdateRequest
     * @return
     */
    @PostMapping("/update")
    public Object updateUser(@RequestBody UserUpdateRequest userUpdateRequest) {
        if (userUpdateRequest == null || userUpdateRequest.getId() == null) {
            throw new ApiException(BaseCode.ParamterValueError);
        }
        SysUser user = new SysUser();
        BeanUtils.copyProperties(userUpdateRequest, user);
        boolean result = userService.updateById(user);
        ThrowUtils.throwIf(!result, BaseCode.InnerError, "删除失败");
        return BaseResponse.ok(true);
    }

    /**
     * 获取用户个人信息
     * @param request
     * @return
     */
    @PostMapping("/getUserInfo")
    public Object getUserInfo(HttpServletRequest request) {
        SysUser user = (SysUser) request.getAttribute("user");
        UserVo userInfo = userService.getUserInfo(user);
        return BaseResponse.ok(userInfo);
    }


}
