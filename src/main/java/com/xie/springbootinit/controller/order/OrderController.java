package com.xie.springbootinit.controller.order;


import com.xie.springbootinit.base.respose.BaseResponse;
import com.xie.springbootinit.model.vo.order.OrderVo;
import com.xie.springbootinit.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 用户下单
     * @param ordersVo
     * @return
     */
    @PostMapping("/submit")
    public Object submit(@RequestBody OrderVo ordersVo) {
        log.info("订单数据：{}", ordersVo);
        orderService.submit(ordersVo);
        return BaseResponse.ok("下单成功");
    }
}