package com.xie.springbootinit.controller.test;

import org.springframework.web.bind.annotation.*;

/**
 * @Author: Xrx
 * @Description: 测试联通类
 * @CreateTime: 2023/5/13 10:22
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/test1")
    public Object test1() {
        return "God Job! 网络连接成功!";
    }

}
