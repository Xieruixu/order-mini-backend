package com.xie.springbootinit.constant;

/**
 * 文件常量
 *
 */
public interface FileConstant {

    /**
     * 图片后缀
     */
    String IMG_SUFFIX = ".jpg";

    /**
     * 默认菜品图片
     */
    String DEFAULT_DISH_IMG = "default_dish_img.jpg";

    /**
     * 默认头像
     */
    String DEFAULT_AVATAR = "default_avatar.jpg";


}
