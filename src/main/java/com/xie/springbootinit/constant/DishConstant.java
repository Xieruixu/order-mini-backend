package com.xie.springbootinit.constant;

/**
 * @Author: Xrx
 * @Description:
 * @CreateTime: 2023/6/21 16:24
 */
public interface DishConstant {

    /**
     * 菜品前缀
     */
    String DISH_PREFIX = "dish:";

}
