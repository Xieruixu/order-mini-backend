package com.xie.springbootinit.constant;

/**
 * @Author: Xrx
 * @Description:
 * @CreateTime: 2023/5/13 14:18
 */
public interface GlobalConstant {

    /**
     * 盐值，混淆密码
     */
    String SALT = "askhuundlksnqiwoindinssacs";

    /**
     * JWT 过期时间
     */
    long TIME = 1000 * 60 * 60L;

    /**
     * JWT 秘钥
     */
    String SIGNATURE = "fdszguikfhdsiufhsldfiaaa";

    /**
     * Token前缀
     */
    String TOKEN_PREFIX = "token: ";


}
