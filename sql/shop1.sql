/*
 Navicat Premium Data Transfer

 Source Server         : StartAI
 Source Server Type    : MySQL
 Source Server Version : 50740 (5.7.40-log)
 Source Host           : 43.140.251.43:3306
 Source Schema         : shop1

 Target Server Type    : MySQL
 Target Server Version : 50740 (5.7.40-log)
 File Encoding         : 65001

 Date: 18/06/2023 18:57:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` bigint(20) NOT NULL COMMENT '分类id',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类类型 1 菜品分类 2 套餐分类',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类名称',
  `sort` int(11) NOT NULL COMMENT '排序',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0 未删除 1 已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category
-- ----------------------------

-- ----------------------------
-- Table structure for dish
-- ----------------------------
DROP TABLE IF EXISTS `dish`;
CREATE TABLE `dish`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜品描述',
  `price` decimal(10, 2) NOT NULL COMMENT '价格',
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜品图片地址',
  `category_id` bigint(20) NOT NULL COMMENT '菜品类别',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(4) NOT NULL DEFAULT 0 COMMENT '逻辑删除 0-未删 1-已删',
  `status` int(11) NOT NULL COMMENT '状态 0-上架 1-下架',
  `sort` int(20) NOT NULL COMMENT '顺序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dish
-- ----------------------------

-- ----------------------------
-- Table structure for dish_flavor
-- ----------------------------
DROP TABLE IF EXISTS `dish_flavor`;
CREATE TABLE `dish_flavor`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `dish_id` int(11) NOT NULL COMMENT '菜品id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '口味名称',
  `value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '口味数据',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0-否 1-是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dish_flavor
-- ----------------------------

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '员工姓名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
  `sex` tinyint(4) NOT NULL COMMENT '性别 0:女 1:男',
  `id_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '身份证号',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态 0:离职 1:在职',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------

-- ----------------------------
-- Table structure for order_detail
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单名称',
  `order_id` bigint(20) NOT NULL COMMENT '订单id',
  `dish_id` bigint(20) NOT NULL COMMENT '菜品id',
  `set_meal_id` bigint(20) NOT NULL COMMENT '套餐id',
  `dish_flavor_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜品口味id',
  `number` int(11) NOT NULL COMMENT '数量',
  `amount` decimal(10, 2) NOT NULL COMMENT '金额',
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0-否 1-是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_detail
-- ----------------------------

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `order_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '订单号',
  `status` int(11) NOT NULL COMMENT '订单状态 0:待支付 1:已支付 2:已取消',
  `user_id` bigint(20) NOT NULL COMMENT '下单用户id',
  `orderTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `checkout_time` datetime NULL DEFAULT NULL COMMENT '支付时间',
  `pay_method` int(11) NOT NULL COMMENT '支付方式 0:微信 1:支付宝 2:银联',
  `amount` decimal(10, 2) NOT NULL COMMENT '实收金额',
  `remark` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0:否 1:是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------

-- ----------------------------
-- Table structure for roles_menus
-- ----------------------------
DROP TABLE IF EXISTS `roles_menus`;
CREATE TABLE `roles_menus`  (
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单权限ID'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of roles_menus
-- ----------------------------
INSERT INTO `roles_menus` VALUES (1, 1);
INSERT INTO `roles_menus` VALUES (1, 2);
INSERT INTO `roles_menus` VALUES (1, 3);
INSERT INTO `roles_menus` VALUES (1, 4);
INSERT INTO `roles_menus` VALUES (1, 5);
INSERT INTO `roles_menus` VALUES (3, 1);
INSERT INTO `roles_menus` VALUES (7, 6);
INSERT INTO `roles_menus` VALUES (2, 1);
INSERT INTO `roles_menus` VALUES (2, 2);
INSERT INTO `roles_menus` VALUES (2, 3);
INSERT INTO `roles_menus` VALUES (2, 4);
INSERT INTO `roles_menus` VALUES (2, 5);
INSERT INTO `roles_menus` VALUES (2, 6);
INSERT INTO `roles_menus` VALUES (5, 1);
INSERT INTO `roles_menus` VALUES (5, 3);
INSERT INTO `roles_menus` VALUES (5, 4);
INSERT INTO `roles_menus` VALUES (6, 1);
INSERT INTO `roles_menus` VALUES (6, 5);
INSERT INTO `roles_menus` VALUES (6, 6);

-- ----------------------------
-- Table structure for roles_permissions
-- ----------------------------
DROP TABLE IF EXISTS `roles_permissions`;
CREATE TABLE `roles_permissions`  (
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `permission_id` bigint(20) NULL DEFAULT NULL COMMENT '数据权限ID'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of roles_permissions
-- ----------------------------
INSERT INTO `roles_permissions` VALUES (3, 1);
INSERT INTO `roles_permissions` VALUES (3, 2);
INSERT INTO `roles_permissions` VALUES (3, 4);
INSERT INTO `roles_permissions` VALUES (3, 15);
INSERT INTO `roles_permissions` VALUES (3, 16);
INSERT INTO `roles_permissions` VALUES (7, 1);
INSERT INTO `roles_permissions` VALUES (7, 2);
INSERT INTO `roles_permissions` VALUES (7, 5);
INSERT INTO `roles_permissions` VALUES (7, 8);
INSERT INTO `roles_permissions` VALUES (2, 1);
INSERT INTO `roles_permissions` VALUES (2, 2);
INSERT INTO `roles_permissions` VALUES (2, 3);
INSERT INTO `roles_permissions` VALUES (2, 4);
INSERT INTO `roles_permissions` VALUES (2, 7);
INSERT INTO `roles_permissions` VALUES (2, 6);
INSERT INTO `roles_permissions` VALUES (2, 5);
INSERT INTO `roles_permissions` VALUES (2, 8);
INSERT INTO `roles_permissions` VALUES (2, 9);
INSERT INTO `roles_permissions` VALUES (2, 10);
INSERT INTO `roles_permissions` VALUES (2, 13);
INSERT INTO `roles_permissions` VALUES (2, 12);
INSERT INTO `roles_permissions` VALUES (2, 11);
INSERT INTO `roles_permissions` VALUES (2, 15);
INSERT INTO `roles_permissions` VALUES (2, 16);
INSERT INTO `roles_permissions` VALUES (2, 19);
INSERT INTO `roles_permissions` VALUES (5, 1);
INSERT INTO `roles_permissions` VALUES (5, 2);
INSERT INTO `roles_permissions` VALUES (5, 4);
INSERT INTO `roles_permissions` VALUES (5, 15);
INSERT INTO `roles_permissions` VALUES (5, 16);
INSERT INTO `roles_permissions` VALUES (6, 1);
INSERT INTO `roles_permissions` VALUES (6, 2);
INSERT INTO `roles_permissions` VALUES (6, 4);
INSERT INTO `roles_permissions` VALUES (6, 15);
INSERT INTO `roles_permissions` VALUES (6, 16);
INSERT INTO `roles_permissions` VALUES (6, 1);
INSERT INTO `roles_permissions` VALUES (6, 2);
INSERT INTO `roles_permissions` VALUES (6, 4);
INSERT INTO `roles_permissions` VALUES (6, 15);
INSERT INTO `roles_permissions` VALUES (6, 16);
INSERT INTO `roles_permissions` VALUES (6, 1);
INSERT INTO `roles_permissions` VALUES (6, 2);
INSERT INTO `roles_permissions` VALUES (6, 4);
INSERT INTO `roles_permissions` VALUES (6, 15);
INSERT INTO `roles_permissions` VALUES (6, 16);
INSERT INTO `roles_permissions` VALUES (6, 5);
INSERT INTO `roles_permissions` VALUES (6, 8);
INSERT INTO `roles_permissions` VALUES (6, 11);

-- ----------------------------
-- Table structure for set_meal
-- ----------------------------
DROP TABLE IF EXISTS `set_meal`;
CREATE TABLE `set_meal`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `category_id` bigint(20) NOT NULL COMMENT '分类id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '套餐名称',
  `price` decimal(10, 2) NOT NULL COMMENT '套餐价格',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态 0:下架 1:上架',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '套餐编码',
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '套餐描述',
  `image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '套餐图片',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0:否 1:是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of set_meal
-- ----------------------------

-- ----------------------------
-- Table structure for set_meal_dish
-- ----------------------------
DROP TABLE IF EXISTS `set_meal_dish`;
CREATE TABLE `set_meal_dish`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `set_meal_id` bigint(20) NOT NULL COMMENT '套餐id',
  `dish_id` bigint(20) NOT NULL COMMENT '菜品id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜品名称',
  `price` decimal(10, 2) NOT NULL COMMENT '菜品价格',
  `copies` int(11) NOT NULL COMMENT '份数',
  `sort` int(11) NOT NULL COMMENT '排序',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0-否 1-是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of set_meal_dish
-- ----------------------------

-- ----------------------------
-- Table structure for shopping_cart
-- ----------------------------
DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE `shopping_cart`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `dish_id` bigint(20) NOT NULL COMMENT '菜品id',
  `setmeal_id` bigint(20) NOT NULL COMMENT '套餐id',
  `dish_flavor` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜品口味',
  `number` int(11) NOT NULL COMMENT '数量',
  `amount` decimal(10, 2) NOT NULL COMMENT '金额',
  `image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图片',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0-否 1-是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shopping_cart
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单路径',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `component` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单组件',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父级菜单',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '显示状态(0不显示、1显示)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '/', 'el-icon-set-up', '系统管理', 'home', NULL, 1);
INSERT INTO `sys_menu` VALUES (2, '/system/user', 'fa fa-user-md', '用户管理', 'system/user/index', 1, 1);
INSERT INTO `sys_menu` VALUES (3, '/system/role', 'fa fa-user-circle-o', '角色管理', 'system/role', 1, 1);
INSERT INTO `sys_menu` VALUES (4, '/system/permission', 'fa fa-pied-piper-alt', '权限管理', 'system/permission', 1, 1);
INSERT INTO `sys_menu` VALUES (5, '/system/menu', 'el-icon-menu', '菜单管理', 'system/menu', 1, 1);
INSERT INTO `sys_menu` VALUES (6, '/', 'eiconfont e-icon-yundong-yumaoqiu', '运动管理', 'home', NULL, 1);
INSERT INTO `sys_menu` VALUES (7, '/sport/intorduction', 'fa fa-codepen', '运动咨询', 'sport/intorduction', 6, 1);
INSERT INTO `sys_menu` VALUES (8, 'food/type/index', 'fa fa-arrow-left', '食物分类管理', 'food/index', 8, 1);
INSERT INTO `sys_menu` VALUES (11, '/', 'fa fa-cutlery', '食物管理', 'home', NULL, 1);
INSERT INTO `sys_menu` VALUES (12, '/food/type', 'fa fa-codepen', '食物的分类管理', 'food/index', 11, 1);
INSERT INTO `sys_menu` VALUES (13, '/sport/motion', 'fa fa-odnoklassniki', '运动项目', 'sport/Motion', 6, 1);
INSERT INTO `sys_menu` VALUES (14, '/sport/goods', 'fa fa-modx', '运动商品', 'sport/Goods', 6, 1);

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标签',
  `code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据权限标签值',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '显示状态(0不显示、1显示)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (1, '添加用户', 'USER_INSERT', 1);
INSERT INTO `sys_permission` VALUES (2, '修改用户', 'USER_UPDATE', 1);
INSERT INTO `sys_permission` VALUES (3, '删除用户', 'USER_DELETE', 1);
INSERT INTO `sys_permission` VALUES (4, '查询用户', 'USER_SELECT', 1);
INSERT INTO `sys_permission` VALUES (5, '设置用户为管理员', 'USER_SET_ADMIN', 1);
INSERT INTO `sys_permission` VALUES (6, '添加角色', 'PRE_ROLE_INSERT', 1);
INSERT INTO `sys_permission` VALUES (7, '修改角色', 'PRE_USER_UPDATE', 1);
INSERT INTO `sys_permission` VALUES (8, '删除角色', 'PRE_USER_DELETE', 1);
INSERT INTO `sys_permission` VALUES (9, '查询角色', 'PRE_USER_SELECT', 1);
INSERT INTO `sys_permission` VALUES (10, '添加权限数据', 'PRE_INSERT', 1);
INSERT INTO `sys_permission` VALUES (11, '修改权限数据', 'PRE_UPDATE', 1);
INSERT INTO `sys_permission` VALUES (12, '删除权限数据', 'PRE_DELETE', 1);
INSERT INTO `sys_permission` VALUES (13, '查询权限数据', 'PRE_SELECT', 1);
INSERT INTO `sys_permission` VALUES (14, '添加菜单', 'MENU_INSERT', 0);
INSERT INTO `sys_permission` VALUES (15, '修改菜单', 'MENU_UPDATE', 1);
INSERT INTO `sys_permission` VALUES (16, '删除菜单', 'MENU_DELETE', 1);
INSERT INTO `sys_permission` VALUES (19, '查询菜单', 'MENU_SELECT', 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色对应的标签值',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '显示状态(0不显示、1显示)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'SUPER_ADMIN', 1);
INSERT INTO `sys_role` VALUES (2, '管理员', 'ADMIN', 1);
INSERT INTO `sys_role` VALUES (3, '普通角色', 'PT_ROLE', 1);
INSERT INTO `sys_role` VALUES (5, '食物分配管理师', 'SPORT', 1);
INSERT INTO `sys_role` VALUES (6, '主任', 'ZHUREN', 1);
INSERT INTO `sys_role` VALUES (7, '搜索', 'fdsf', 0);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `sex` tinyint(1) NULL DEFAULT NULL COMMENT '性别(0男，1女，2未知)',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `open_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信小程序openid，每个用户对应一个，且唯一',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '状态，是否禁用 0-未禁用 1-已禁用',
  `admin` tinyint(1) NULL DEFAULT NULL COMMENT '是否是管理员',
  `phone_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话号码',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户邮箱',
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(4) NULL DEFAULT 0 COMMENT '是否删除 0-未除 1-已删',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_open_id`(`open_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles`  (
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES (2, 3);
INSERT INTO `user_roles` VALUES (3, 5);
INSERT INTO `user_roles` VALUES (3, 1);

SET FOREIGN_KEY_CHECKS = 1;
